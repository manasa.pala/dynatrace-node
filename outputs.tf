output "connect_to_dynatrace" {
  value = "https://${aws_instance.dynatracemanagednode.public_dns}"
}
