variable "DYNATRACE_DOWNLOAD_URL" {
 default ="https://dynatrace-url"
}

variable "DYNATRACE_LICENSE_KEY"{
 default = "YourOwnLicenseKey"
}

variable "DYNATRACE_INIT_ENV"{
 default = "My5MinutesEnvironment"
}

variable "DYNATRACE_INIT_NAME"{
 default = "YourNameHere"
} 

variable "DYNATRACE_INIT_LASTNAME"{
 default = "YourLastNameHere"
} 
variable "DYNATRACE_INIT_EMAIL"{  
  default = "YourAwesome@Email.com"
}
variable "DYNATRACE_INIT_PWD"{
 default = "YourTemporaryAdminPwd"
}

variable "AWS_ACCESS_KEY" {
  type = string
  default = "YourAccessKeyHere"
}

variable "AWS_SECRET_KEY" {
  type = string
  default = "YourSecretKey"
}

variable "AWS_REGION" {
	default = "eu-west-1"
}

variable "AWS_KEYPAIR_NAME"{
 default = "MyAWSKeyName"
}

variable "AWS_PRIVATE_KEY"{ 
 default = "/path/to/file/MyAWSKeyName.pem"
}

variable "ROOT_VOLUME_SIZE"{
 default = 20
}

variable "AMIS"{
	type = map(string)
	default = {
		us-east-2 = "ami-24260641"
		us-east-1 = "ami-cb1d41b0"
		us-west-1 = "ami-629db402"
		us-west-2 = "ami-29716b50"
		ap-south-1 = "ami-27ec9448"
		ap-northeast-2 = "ami-fdb06993"
		ap-southeast-1 = "ami-ba59cbd9"
		ap-southeast-2 = "ami-c8f3ecab"
		ap-northeast-1 = "ami-014ead67"
		ca-central-1 = "ami-4403bc20"
		eu-central-1 = "ami-1bd17d74"
		eu-west-1 = "ami-254ba35c"
		eu-west-2 = "ami-2819084c"
		sa-east-1 = "ami-cb5027a7"	
	}
}

variable "DYNATRACE_SIZING" {
	default = "trial"
}

variable "AWS_INSTANCE_TYPE" {
	type = "map"
	default = {
		trial = "m4.xlarge" 
		small = "m4.2xlarge"
		medium = "m4.4xlarge"
		large = "m4.10xlarge"
		trial-memory = "r4.xlarge" 
		small-memory = "r4.2xlarge"
		medium-memory = "r4.4xlarge"
		large-memory = "r4.8xlarge"
	}
}
