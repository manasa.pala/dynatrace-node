provider "aws" {
  region = "us-west-1"
}
resource "aws_security_group" "terraformsg" {
  name        = "terraformsg"
  description = "Allow SSH and HTTPS inbound traffic for Dynatrace Managed"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
resource "aws_instance" "dynatracemanagednode" {
	
	vpc_security_group_ids = [aws_security_group.terraformsg.id]
	key_name = var.AWS_KEYPAIR_NAME
  	ami = "${lookup(var.AMIS, var.AWS_REGION)}"
  	instance_type = "${lookup(var.AWS_INSTANCE_TYPE, var.DYNATRACE_SIZING)}"
	root_block_device {
        volume_size = var.ROOT_VOLUME_SIZE
   	}
	tags = {
    	  DynatraceManagedNode = "SpinnedUpin5Minutes"
  	}
	
	provisioner "remote-exec" {
    		inline = [
      			"sudo wget -O /tmp/dynatrace-managed.sh ${var.DYNATRACE_DOWNLOAD_URL}",
      			"cd /tmp/",
				"sudo /bin/sh dynatrace-managed.sh --install-silent --license ${var.DYNATRACE_LICENSE_KEY} --initial-environment ${var.DYNATRACE_INIT_ENV} --initial-first-name ${var.DYNATRACE_INIT_NAME} --initial-last-name ${var.DYNATRACE_INIT_LASTNAME} --initial-email ${var.DYNATRACE_INIT_EMAIL} --initial-pass ${var.DYNATRACE_INIT_PWD}"
    		]
			connection {
				type = "ssh"
				user = "ubuntu"
				private_key = "${file(var.AWS_PRIVATE_KEY)}"
				timeout = "3m"
				agent = false
                host = aws_instance.dynatracemanagednode.id
			}
  	}
}
